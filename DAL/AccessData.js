'use strict';

let MongoClient = require('mongodb').MongoClient,
    ObjectID  = require('mongodb').ObjectID,
    assert = require('assert');

let config = require('config.json')('./config/config.json');

let url = config.mongodb.url;

//let db = new Db('SkybetTest', new Server('localhost', 27017));

let AccessData = class Accessdata {

    checkCollectionExists(collectionName){

        MongoClient.connect(url, function(err, db) {
            db.createCollection(collectionName, function(err, res) {

                if (err) throw err;

                let updateOps = [
                    { updateOne: { filter: {name:  'Jeff', surname:'Stelling' }, update: { $set: {name:'Jeff',surname:'Stelling'}}, upsert:true } },
                    { updateOne: { filter: {name:  'Chris', surname:'Kamara' }, update: { $set: {name:'Chris',surname:'Kamara'}}, upsert:true } },
                    { updateOne: { filter: {name:  'Alex', surname:'Hammond' }, update: { $set: {name:'Alex',surname:'Hammond'}}, upsert:true } },
                    { updateOne: { filter: {name:  'Jim', surname:'White' }, update: { $set: {name:'Jim',surname:'White'}}, upsert:true } },
                    { updateOne: { filter: {name:  'Natalie', surname:'Sawyer' }, update: { $set: {name:'Natalie',surname:'Sawyer'}}, upsert:true } }

                ];

                db.collection(collectionName).bulkWrite(updateOps, {"ordered": true, "w": 1}, function (err, r) {
                    if(err!==null){
                        throw err;
                    }

                    db.close();
                });
            });
        })
    }

    insert(objVal,collectionName,resolve,reject){
        MongoClient.connect(url, function(err, db) {
            db.collection(collectionName).insertOne(objVal, function (err, r) {
                if(err!==null){
                    reject(err.message);
                }
                else{
                    resolve(r);
                }
                db.close();
            });
            return false;
        });
    }

    updateOrInsert(listObjVal,collectionName,resolve, reject){

        MongoClient.connect(url, function(err, db) {
            db.collection(collectionName).deleteMany({},function (err, r) {
                db.collection(collectionName).insertMany(listObjVal, {"ordered": true, "w": 1}, function (err, r) {
                    if(err!==null){
                        db.close();
                        reject(err.message);
                    }
                    resolve(true);
                    db.close();
                });
            });


        });
    }

    get(id,collectionName,resolve,reject){
        id = ObjectID.ObjectID(id);
        MongoClient.connect(url, function(err, db) {
            db.collection(collectionName).findOne({_id: id}, function (err, r) {
                if(err!==null){
                    reject(err.message);
                }
                else{
                    resolve(r);
                }
                db.close();
            });
        });
    }

    list(collectionName,resolve,reject){
        MongoClient.connect(url, function(err, db) {
            db.collection(collectionName).find({}).toArray(function (err, r) {

                if(err!==null){
                    reject(err.message);
                }
                else{
                    resolve(r);
                }
                db.close();
            });
        });
    }
    /*
    private getNextSequence(next){
        var ret = db.counters.findAndModify(
            {
                query: { _id: name },
                update: { $inc: { seq: 1 } },
                new: true
            }
        );

        return ret.seq;
    }
    */
};




module.exports = AccessData;