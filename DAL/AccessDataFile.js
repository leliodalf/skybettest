'use strict';

let fs = require('fs');
let config = require('config.json')('./config/config.json');
let pathFile = config.filePath;

//let db = new Db('SkybetTest', new Server('localhost', 27017));

let AccessDataFile = class AccessdataFile {


    checkCollectionExists(collectionName){
        let file = pathFile+collectionName+'.json';
        if(!fs.existsSync(file)){
            let listUser = [
                {"name":"Jeff","surname":"Stelling"},
                {"name":"Chris","surname":"Kamara"},
                {"name":"Alex","surname":"Hammond"},
                {"name":"Jim","surname":"White"},
                {"name":"Natalie","surname":"Sawyer"},
            ];
            fs.writeFile(file, JSON.stringify(listUser) , (err) => {
                if (err) throw err;
                console.log('The file has been saved!');
            });
        }
    }

    insert(objVal,collectionName,resolve,reject){

        MongoClient.connect(url, function(err, db) {
            db.collection(collectionName).insertOne(objVal, function (err, r) {
                if(err!==null){
                    reject(err.message);
                }
                else{
                    resolve(r);
                }
                db.close();
            });
            return false;
        });
    }

    updateOrInsert(listObjVal,collectionName,resolve, reject){
        let file = pathFile+collectionName+'.json';
        fs.writeFile(file, JSON.stringify(listObjVal), (err) => {

            if (err)throw err;
            resolve(true)
        });

    }

    get(id,collectionName,resolve,reject){
        id = ObjectID.ObjectID(id);
        MongoClient.connect(url, function(err, db) {
            db.collection(collectionName).findOne({_id: id}, function (err, r) {
                if(err!==null){
                    reject(err.message);
                }
                else{
                    resolve(r);
                }
                db.close();
            });
        });
    }

    list(collectionName,resolve,reject){
        let file = pathFile+collectionName+'.json';
        fs.readFile(file,'utf8', (err, data) => {
            if (err) throw err;
            if(data!=='')
                resolve(JSON.parse(data));

            resolve({});
        });
    }
};




module.exports = AccessDataFile;