let assert = require('assert');
let ManagerUser = require('../Manager/ManagerUser');
let User = require('../Model/User');

describe('UserManager', function() {
    describe('#List(file)', function() {
        it('should return a list of 5 users from file', function() {

            let list = function () {
                return new Promise((resolve,reject)=>{
                    let manUser = new ManagerUser('file');
                    manUser.List(resolve,reject);
                });
            };

            return list().then(function (data) {
                assert.equal('object',typeof data);
                assert.equal(data.length,5);

            }).catch(function(error){
                assert.fail()
            });
        });
    });
});

describe('UserManger', function() {
    describe('#UpdateOrInsertMore(file)', function() {
        it('should update a list of users on file and return a boolean', function() {
            let save = function () {

                return new Promise((resolve,reject)=>{
                    let tempUser = [
                        {"name":"Jeff","surname":"Stelling"},
                        {"name":"Chris","surname":"Kamara"},
                        {"name":"Alex","surname":"Hammond"},
                        {"name":"Jim","surname":"White"},
                        {"name":"Natalie","surname":"Sawyer"}
                    ];
                    let listToSave = [];
                    for(let i=0;i<tempUser.length; i++){
                        let user = new User();
                        user.Surname = tempUser[i].surname;
                        user.Name = tempUser[i].name;

                        if(!user.isValid()){
                            throw 'No valid request';
                        }


                        listToSave.push(user);
                    }
                    let manUser = new ManagerUser('file');
                    manUser.UpdateOrInsertMore(listToSave,resolve,reject);

                });
            };

            return save().then(function (data) {
                assert.equal(data,true);
            }).catch(function(error){
                assert.fail();
            });
        });
    });
});


describe('UserManager', function() {
    describe('#List(mongo)', function() {
        it('should return a list of 5 users from mongodb', function() {

            let list = function () {
                return new Promise((resolve,reject)=>{
                    let manUser = new ManagerUser('mongo');
                    manUser.List(resolve,reject);
                });
            };

            return list().then(function (data) {
                assert.equal('object',typeof data);
                assert.equal(data.length,5);

            }).catch(function(error){
                assert.fail()
            });
        });
    });
});

describe('UserManger', function() {
    describe('#UpdateOrInsertMore(mongodb)', function() {
        it('should update a list of users on mongodb and return a boolean', function() {
            let save = function () {

                return new Promise((resolve,reject)=>{
                    let tempUser = [
                        {"name":"Jeff","surname":"Stelling"},
                        {"name":"Chris","surname":"Kamara"},
                        {"name":"Alex","surname":"Hammond"},
                        {"name":"Jim","surname":"White"},
                        {"name":"Natalie","surname":"Sawyer"}
                    ];
                    let listToSave = [];
                    for(let i=0;i<tempUser.length; i++){
                        let user = new User();
                        user.Surname = tempUser[i].surname;
                        user.Name = tempUser[i].name;

                        if(!user.isValid()){
                            throw 'No valid request';
                        }


                        listToSave.push(user);
                    }
                    let manUser = new ManagerUser('mongo');
                    manUser.UpdateOrInsertMore(listToSave,resolve,reject);

                });
            };

            return save().then(function (data) {
                assert.equal(data,true);
            }).catch(function(error){
                assert.fail();
            });
        });
    });
});