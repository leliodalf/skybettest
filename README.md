# SkyBet TEST

This project is developed with NodeJS and ES6. The idea was that it can run on all operating systems and for its
performance and simplicity to run it. MongoDB is used as support for this application, but it is not necessary.
The project is subdivided into multiple folders, each containing a typology of class, model, controllers, views, DAL.
To keep the various features separate

A template engine (JADE) is implemented, as a result of the view index, you get the html form you provided.

I did not touch the structure of the form in html, not knowing if it had to be done, so I did not implement frontend
form input validations

The main modules integrated into NodeJS are: ExpressJS, Prevent Cross-Site Request Forgery (CSURF), Mocha (Unit Test),
 MongoDB Driver, and config.json



 # HOW IT WORKS?

 Two routes are implemented in the project. One to view the list of users in the form fields, the second to manage the save action (routes / index.js)

 The initial route (http://your.domain/) will display the list (GET request)
 The route (http://your.domain/user/save) will update the list of users (POST request)

 The different functions in the classes work with ES6 Promises

 In the configuration file in config / config.json you have to set the different minimum values required to run the application.
 In the file you have to set whether to save the data to files or to MongoDB and possibly the configurations where to save the file or the mongo url

 A prevent Cross-Site Request Forgery is implemented in the project

 A simple Unit Test is implemented in the project (test / test.js)



 # HOW TO INSTALL IT?

 Download and install node (latest version)

 Download and install MongoDb and create a new DB named 'SkyBetTest' (Not necessary, just if you want run the application with mongodb support)

 Go into the project

 Run 'npm install' to install all requests modules

 Into config/config.js set the use of mongo or file for to save the data  "typeAccessData" : "mongo" | "file"

 Run 'node bin/www' (the entry point)

 #For execute the Unit Test

 Run 'npm test'

