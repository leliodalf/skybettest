let express = require('express');
let router = express.Router();
let ManagerUser = require('../Manager/ManagerUser');
let User = require('../Model/User');
let csrf = require('csurf');
let bodyParser = require('body-parser');
let csrfProtection = csrf({ cookie: true });
let parseForm = bodyParser.urlencoded({ extended: false });
/* GET home page. */

let manUser = new ManagerUser();
manUser.init();

router.get('/', csrfProtection, function(req, res, next) {
  let list = function () {
    return new Promise((resolve,reject)=>{
        manUser.List(resolve,reject);
    });
  };

  list().then(function (data) {
      res.render('index', { title: 'Skybet Test', users: data, csrfToken: req.csrfToken() });
  }).catch(function(error){
      res.status(500);
  });

});

router.post('/user/save', parseForm, csrfProtection, function(req, res, next) {
    try{
        if(!req.body.hasOwnProperty("people") && !Array.isArray(req.body.people)){
           throw 'No valid request';
        }


        let save = function () {

            return new Promise((resolve,reject)=>{

                let request = req.body.people[0];

                let countEleFirstname = request.firstname.length;

                let tempUser = [];
                for(let i=0;i<countEleFirstname; i++){
                    let user = new User();
                    user.Surname = request.surname[i];
                    user.Name = request.firstname[i];

                    if(!user.isValid()){
                        throw 'No valid request';
                    }


                    tempUser.push(user);
                }

                manUser.UpdateOrInsertMore(tempUser,resolve,reject);
            });
        };

        save().then(function (data) {
            res.redirect('/');
        }).catch(function(error){
            res.render('index', { title: 'Skybet Test',users:[],error: error });
        });
    }
    catch (e){
        console.log("error" + e);
        res.render('index', { title: 'Express',users:[] });
    }



});




module.exports = router;
