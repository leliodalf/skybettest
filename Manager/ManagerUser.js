'use strict';

let AccessData = require("../DAL/AccessData");
let AccessDataFile = require("../DAL/AccessDataFile");
let config = require('config.json')('./config/config.json');
const COLLECTION = "Users";
let self = {};
let ManagerUser = class ManagerUser{

    constructor(typeAccessData){

        if(typeAccessData===undefined){
            typeAccessData = config.typeAccessData;
        }

        if(typeAccessData==="mongo"){
            self.userData = new AccessData();
        }
        else if(typeAccessData==="file"){
            self.userData = new AccessDataFile();
        }
        else{
            throw "The access data configuration must be mongo or file";
        }
    }

    init(){
        self.userData.checkCollectionExists(COLLECTION);
    }

    Insert(user){

        let save = function(){
            return new Promise((resolve, reject)=>{
                this.userData.insert(user.toJSON(),COLLECTION,resolve, reject);
            });
        };

        save().then(function(data){
            console.log(data);
        });

    }

    UpdateOrInsertMore(listUser,resolve, reject){
        let save = function(){
            return new Promise((resolve, reject)=>{
                let listToSave = listUser.map((user) => {
                    return user.toJSON();
                });
                self.userData.updateOrInsert(listToSave,COLLECTION,resolve, reject);
            });
        };

        save().then(function(data){
            resolve(data);
        }).catch(function (error) {
            reject(error);
        });
    }

    Get(id){

        let get = function(){
            return new Promise((resolve, reject)=>{
                this.userData.get(id,COLLECTION,resolve, reject);
            });
        };

        get().then(function (data) {
            console.log(data);
        }).catch(function(error){
            console.log(error)
        });


    }

    List(resolve, reject){
        let list = function(){
            return new Promise((resolve, reject)=>{

                self.userData.list(COLLECTION, resolve, reject);
            });
        };
        list().then(function (data) {

            resolve(data);
        }).catch(function(error){
            reject(error);
        });
    }
}

module.exports = ManagerUser;

