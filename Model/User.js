'use strict';
let User = class User {

    get Name() { return this.name; }
    set Name(name) {this.name = name;}



    get Surname() {return this.surname; }
    set Surname(surname) {this.surname = surname; }

    isValid() {
        return !(this.surname === undefined || this.surname === '' || this.name === undefined || this.name === '');
    }

    toJSON(){
        return {name:this.name,surname:this.surname};
    }

}

module.exports = User;
